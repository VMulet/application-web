'use strict';
const db = require('../modules/db');
const debug = require('debug')('sigfox-callback:request-logger');
module.exports = function(req, res, next){
    let entry = { //extract useful data of URL ( callback )
	method: req.method,
	path: req.url.replace(/\?(.*)$/, ''),
	time: new Date().getTime(),
	payload:req.body||{}
    };
    function hex2ascii(received) //convert hexadecimal code to ASCII
    {
	var hex = received.toString();
	var str = '';
	for (var i=0; i<hex.length; i+=2)
	    str+=String.fromCharCode(parseInt(hex.substr(i, 2), 16));
	return str;
    }

    if(entry.payload.data)// use hex2ascii if the callback has a variable named data and split the message on two variable ( Battery and Arduino error )
    {
	var data = hex2ascii(entry.payload.data);
	var batterie='';
	var Arduino='';
	for(var i=0;i<data.length;i++)
	{
	    if(i<2){ batterie+=data[i]; }
	    else{ Arduino+=data[i]; }
	}
	entry.payload["battery"]=batterie;
	entry.payload["Arduino_error"]=Arduino;
	delete entry.payload.data;
    }
    db.find('calls', {path:'/sigfox',"payload.time":entry.payload.time,"payload.id":entry.payload.id}, {sort:{time:-1}})//regroup callback in the database
        .then(function(database)
	      {
		  database.forEach(function(data)
				   {
					   if(entry.payload.lat)
					   {
					       console.log("upload de la partie batterie");
					       entry.payload["battery"]=data.payload.battery;
					       entry.payload["Arduino_error"]=data.payload.Arduino_error;
					       console.log("upload de la partie batterie done");
					   }
					   else
					   {
					       console.log("upload de la partie lat-lng");
					       entry.payload["snr"]=data.payload.snr;
					       entry.payload["station"]=data.payload.station;
					       entry.payload["avgSignal"]=data.payload.avgSignal;
					       entry.payload["rssi"]=data.payload.rssi;
					       entry.payload["lat"]=data.payload.lat;
					       entry.payload["lng"]=data.payload.lng;
					       entry.payload["radius"]=data.payload.radius;
					       console.log("upload de la partie lat-lng done")
					   }
				   })
		  //querystring ?
		  if (req.query && Object.keys(req.query).length){//extract payload in request
		      Object.keys(req.query).forEach(function(key){
			  if (!entry.payload[key]){
			      entry.payload[key] = req.query[key];
			  }
			  else{
			      debug('WARN GET/POST conflict on %s', key);
			      entry.payload['qry-'+key] = req.query[key];
			  }
		      });
		  }
		  console.log("work done");
		  db.insert('calls', entry)//insert data in database
		      .then(function(obj){
			  debug('Request log OK');
			  debug(obj);
			  next();
		      })
		      .catch(function(err){
			  debug('Log err : %s', err);
			  //return res.status(500).json({err:'Unable to log request', details:err.message});
			  next();
		      });
	      });
};
