
'use strict';
/**
* Load local config to env
*/
try{
    const localConfig = require('./config.local.js');
    for (let entry in localConfig){
	if (process.env[entry]){
	    console.log('%s found in process.env too, ignore the local config val\n\t env vars always have precedence', entry);
	}
	else{
	    process.env[entry] = localConfig[entry];
	}
    }
}
catch(e){
    console.log('No local config found');
    console.log(e);
}

const debug = require('debug')('sigfox-callback:app');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const path = require('path');
/* init */
const app = express();
const port = process.env.PORT || 8080;
const server = http.createServer(app);
const db = require('./modules/db');
const requestLogger = require('./middlewares/requestLogger');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended:false}));
app.locals.moment = require('moment');


db.connect();
server.listen(port);

app.get('/', function(req, res){
    debug('Looking for logs');
    db.find('calls', {path:'/sigfox', payload:{$exists:true}}, {sort:{time:-1}})
	.then(function(data){	    
	    debug('%s items found', data.length);
	    data.forEach(function(osf){
		console.log(osf);
	    });
	    
	    res.format({
		/* JSON first */
		json: function(){
		    res.json({entries:data});
		},
		html: function(){
		    res.render('sigfox-logs', {title:'SIGFOX messages', entries:data});
		},
		default:function(){
		    res.status(406).send({err:'Invalid Accept header. This method only handles html & json'});
		}
	    });
	})
	.catch(function(err){
	    res.format({
		json: function(){
		    return res.json({err:'An error occured while fetching messages', details:err});
		},
		html: function(){
		    return res.status(500).render('error', {title:'An error occured while fetching messages', err:err});
		},
		default: function(){
		    res.status(406).send({err:'Invalid Accept header. This method only handles html & json'});
		}
	    });
	});
});

app.get('/test', function(req,res){
    db.find('calls',{path:'/sigfox',payload:{$exists:true}},{sort:{time:-1}})
	.then(function(data){
	    res.format({
		html:function(){
		    res.render('test',{title:'SIGFOX messages', entries:data});
		},
		default:function(){
		    res.status(406).send({err:'Invalid Accept header. This method only handles html'});
		}
	    });
	})
	.catch(function(err){
	    res.format({
		html: function(){
		    return res.status(500).render('error', {title:'An error occured while fetching messages', err:err});
		},
		default: function(){
		    res.status(406).send({err:'Invalid Accept header. This method only handles html'});
		}
	    });
	});
});

app.post('/sigfox', requestLogger, function(req, res){
  console.log(hex2ascii(req.body));    
  debug('~~ POST request ~~');
  res.json({result:'♡'});
});

function ErrorReception()
{
    var result=[];
    var d=new Date();
    var tmp;
    var idx=1;
    var last;
    console.log("iogiigfopfjĵf^");
    db.find('calls',{path:'/sigfox'})
        .then(function(database)
              {
                  database.forEach(function(data){
                      console.log(data);
                      /*if(idx==1){
                          last=data.time;
                      }
                      if(idx==database.length)
                      {
                          if(Math.trunc(((d.getTime()-data.time))/60000)>=17)
                          {

                              tmp=(Math.trunc((d.getTime()-data.time)/900000));
                              result.push({start: data.time ,current_time:d.getTime(),error:tmp})
                              //console.log("current",d.getTime());
                              //console.log("data",data.time);
                          }
                      }
                      else{
                          if(Math.trunc(((data.time-last))/60000)>=16)
                          {
                              //console.log(data.time-last);
                              tmp=(Math.trunc((data.time-last)/90000));
                              result.push({start: last,end:data.time,tmp})
                          }
                      }*/
                      //idx++;
                      //last=data.time;
                  });
                  //console.log(d.getTime()-last);
                  //console.log(result);
                  return(result);
              });
}
ErrorReception(); 
/*function hex2ascii(received)
{
    var hex = received.toString();
    var str = '';
    for (var i=0; i<hex.length; i+=2)
	str+=String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function decoder(req, res, next) {
    // decoder
    // translate the byte array into key value pairs
    // remove the byte array reference
    next(req, res);
}
*//*
app.post('/sigfox', decoder, function(req, res){
    requestLogger(req, res, null);
    debug('~~ POST request ~~');
    res.json({result:'♡'});
});*/
server.on('error', function(err){
    debug('ERROR %s', err);
});
server.on('listening', function(){
    debug('Server listening on port %s', port);
});
